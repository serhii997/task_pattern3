package com.movchan.model.decorator;

import com.movchan.model.typeFlower.Flower;

import java.util.Optional;

public class FlowerDecorator implements Flower {
    private Optional<Flower> flower;
    private int additionalPrice;
    private String additionalComponent;

    public void setFlower(Flower outFlower) {
        flower = Optional.ofNullable(outFlower);
        if (additionalComponent != null) {
            flower.orElseThrow(IllegalAccessError::new).getName().concat(additionalComponent);
        }
    }

    public void setAdditionalPrice(int additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setAdditionalComponent(String additionalComponent) {
        this.additionalComponent = additionalComponent;
    }

    @Override
    public String getName() {
        return flower.orElseThrow(IllegalAccessError::new).getName();
    }

    @Override
    public int getPrice() {
        return flower.orElseThrow(IllegalAccessError::new).getPrice() + additionalPrice;
    }

    @Override
    public int getCount() {
        return flower.orElseThrow(IllegalAccessError::new).getPrice();
    }

    @Override
    public void setCount(int count) {

    }

//    @Override
//    public TypeDelivery getDelivery() {
//        return flower.orElseThrow(IllegalAccessError::new).getDelivery();
//    }
//
//    @Override
//    public TypePackaging getPackaging() {
//        return flower.orElseThrow(IllegalAccessError::new).getPackaging();
//    }
}
