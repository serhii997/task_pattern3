package com.movchan.model.typeFlower.bouquets;

import com.movchan.model.decorator.TypeDelivery;
import com.movchan.model.decorator.TypePackaging;
import com.movchan.model.typeFlower.Bouquet;
import com.movchan.model.typeFlower.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyBouquet implements Bouquet {
    private static final Logger LOGGER = LogManager.getLogger(MyBouquet.class);

    private List<Flower> flowers = new ArrayList<>();
    private String nameBouquet;
    private int bouquetPrice;
    private TypeDelivery delivery;
    private TypePackaging packaging;

    public MyBouquet(String nameBouquet, Flower ...flower) {
        this.nameBouquet = nameBouquet;
        makeBouquet(flower);
        getBouquetPrice();
    }



//    public List<Flower> addFlower(Flower ...flower) {
//        flowers.addAll(Arrays.asList(flower));
//        return flowers;
//    }

    public int getPrice() {
        return bouquetPrice;
    }

    private void getBouquetPrice() {
        for ( Flower flower : flowers ) {
            bouquetPrice += (flower.getPrice() * flower.getCount());
        }
    }

    private void makeBouquet(Flower ...flower) {
        flowers.addAll(Arrays.asList(flower));
    }


    @Override
    public String toString() {
        return "MyBouquet{" +
                "flowers=" + flowers +
                ", nameBouquet='" + nameBouquet + '\'' +
                ", bouquetPrice=" + bouquetPrice +
                '}';
    }

    @Override
    public TypeDelivery getDelivery() {
        return delivery;
    }

    @Override
    public TypePackaging getPackaging() {
        return packaging;
    }
}
