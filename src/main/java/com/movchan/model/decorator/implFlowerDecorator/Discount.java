package com.movchan.model.decorator.implFlowerDecorator;

import com.movchan.model.decorator.FlowerDecorator;

public class Discount extends FlowerDecorator{
    private final int ADDITIONAL_PRICE = -30;
    private final String ADDITIONAL_COMPONENT = "added Delivery";

    public Discount() {
        setAdditionalComponent(ADDITIONAL_COMPONENT);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }

}
