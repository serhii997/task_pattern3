package com.movchan.model.typeFlower.flowers;

import com.movchan.model.Event;
import com.movchan.model.typeFlower.Flower;

import java.util.List;

public class Tulip implements Flower {
    private String name;
    private int price;
    private int count;
    private List<Event> types;

    public Tulip(String name, int price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Tulip{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
