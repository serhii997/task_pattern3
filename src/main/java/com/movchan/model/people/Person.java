package com.movchan.model.people;

public interface Person {
    String getName();
    double getCash();
}
