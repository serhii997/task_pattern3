package com.movchan.model.typeFlower.bouquets;

import com.movchan.model.people.domain.Client;
import com.movchan.model.typeFlower.Flower;
import com.movchan.model.typeFlower.flowers.Daisy;
import com.movchan.model.typeFlower.flowers.Rose;
import com.movchan.model.typeFlower.flowers.Tulip;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class AllBouquet {
    private static final Logger LOGGER = LogManager.getLogger(MyBouquet.class);
    private List<MyBouquet> bouquets = new ArrayList<>();
    private List<Flower> myBouquets = new ArrayList<>(); //?
    private List<Flower> flowers = new ArrayList<>();
    private Rose grandPrix;
    private Tulip yellow;
    private Daisy field;

    public AllBouquet() {
        bouquets.add(new MyBouquet("Summer Bouquet", new Daisy("Wood", 15, 15),
                new Rose("Field", 18, 12)));
        bouquets.add(new MyBouquet("Rose bouquet", new Rose("White", 25, 21),
                new Rose("Red", 35, 40)));
        bouquets.add(new MyBouquet("Field bouquet", new Daisy("Field", 15, 14),
                new Daisy("Garden", 25, 11)));
        bouquets.add(new MyBouquet("Spring bouquets", new Tulip("Yellow", 25, 17),
                new Tulip("Purple", 25, 12)));
        сompositionFlowers();
    }

    public void printAllBouquets() {
        bouquets.forEach(LOGGER::info);
    }

    public void printAllFlowers() {
        flowers.forEach(LOGGER::info);
    }

    private void сompositionFlowers() {
        grandPrix = new Rose("Grand Prix", 45, 100);
        yellow = new Tulip("Yellow", 22, 100);
        field = new Daisy("Field", 15, 100);
        flowers.addAll(Arrays.asList(grandPrix, yellow, field));
    }

    public void boughtBouquet(Scanner scanner, Client client) {
        while (true) {
            System.out.println("which flower: 1 - rose, 2 - tulip, 3 - daisy, q - exit");
            int choose = scanner.nextInt();
            if (choose == 1) {
                System.out.println("Please input count flower");
                int count = scanner.nextInt();
                addFlower(client, grandPrix, count);
            } else if (choose == 2) {
                System.out.println("Please input count flower");
                int count = scanner.nextInt();
                addFlower(client, yellow, count);
            } else if (choose == 3) {
                System.out.println("Please input count flower");
                int count = scanner.nextInt();
                addFlower(client, field, count);
            } else {
                break;
            }
        }
    }

    private void addFlower(Client client, Flower flower, int count) {
        int sumCash = count * flower.getPrice();
        if ((count <= flower.getCount()) && (client.getCash() >= sumCash)) {
            flower.setCount(flower.getCount() - count);
            client.setCash(client.getCash() - (count * flower.getPrice()));
        } else {
            LOGGER.error("The store doesn't have " + count + " many flowers or don't have money");
        }
    }

    public void chooseBouqets(Client client, int index) {
        if (client.getCash() >= bouquets.get(index).getPrice()) {
            client.setCash(client.getCash() - bouquets.get(index).getPrice());
            bouquets.remove(index);
        } else {
            LOGGER.error("Incorrect index or don't have money");
        }
    }

    private void createBouquet(Flower... flower) {
        myBouquets.addAll(Arrays.asList(flower));
    }


}
