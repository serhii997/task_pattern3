package com.movchan.model.typeFlower;

import com.movchan.model.decorator.TypeDelivery;
import com.movchan.model.decorator.TypePackaging;

public interface Bouquet {
    TypeDelivery getDelivery();
    TypePackaging getPackaging();
}
