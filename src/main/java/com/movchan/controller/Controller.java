package com.movchan.controller;

import java.util.Scanner;

public interface Controller {
    void addClient(Scanner scanner);
    void showClients();
    void chooseClient(Scanner scanner);
    void showFlowers();
    void showBouquets();
    void createOrder(Scanner scanner);
    void addFlowers(Scanner scanner);
}
