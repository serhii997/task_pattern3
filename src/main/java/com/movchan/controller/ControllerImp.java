package com.movchan.controller;

import com.movchan.model.people.domain.Client;
import com.movchan.model.typeFlower.Flower;
import com.movchan.model.typeFlower.bouquets.AllBouquet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ControllerImp implements Controller {
    private static final Logger LOGGER = LogManager.getLogger(ControllerImp.class);

    private AllBouquet allBouquet;
    private Flower flower;
    List<Client> clients = new ArrayList<>();
    private Client sergio;
    private Client orest;
    private Client client; //тоді клієнт може вибирати квіти

    public ControllerImp() {
        allBouquet = new AllBouquet();
        sergio = new Client("Sergio", 1000);
        orest = new Client("Orest", 1000);
        clients.addAll(Arrays.asList(sergio, orest));
    }

    @Override
    public void addClient(Scanner scanner) {
        System.out.println("Please input your name");
        String name = scanner.next();
        System.out.println("Please input your cash");
        int cash = scanner.nextInt();
        clients.add(new Client(name, cash));
    }

    @Override
    public void showClients() {
        clients.forEach(LOGGER::info);
    }

    @Override
    public void chooseClient(Scanner scanner) {//тоді клієнт може вибирати квіти
        clients.forEach(LOGGER::info);
        try {
            System.out.println("Please input index for client: ");
            client = clients.get(scanner.nextInt() - 1);
            LOGGER.info(client);
        } catch (IndexOutOfBoundsException e) {
            LOGGER.warn(e);
            LOGGER.info("Please input correct index");
        }

    }

    @Override
    public void showFlowers() {
        allBouquet.printAllFlowers();
    }

    @Override
    public void showBouquets() {
        allBouquet.printAllBouquets();
    }

    @Override
    public void createOrder(Scanner scanner) {
        if (client != null) {
                System.out.println("1 - bouquets" + "\n" + "2 - flowers");
            int choose = scanner.nextInt();
            if (choose == 1) {
                allBouquet.printAllBouquets();
                System.out.println("Choose bouquets");
                int index = scanner.nextInt();
                allBouquet.chooseBouqets(client, index);
            } else if (choose == 2) {
                allBouquet.printAllFlowers();
                allBouquet.boughtBouquet(scanner, client);
            }

        } else {
            LOGGER.info("you can't do order, because you don't login ");
        }
    }

    @Override
    public void addFlowers(Scanner scanner) {
        allBouquet.boughtBouquet(scanner, sergio);
    }
}
