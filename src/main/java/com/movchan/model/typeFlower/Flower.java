package com.movchan.model.typeFlower;

public interface Flower {
    String getName();
    int getPrice();
    int getCount();
    void setCount(int count);
}
