package com.movchan.model.people.domain;

import com.movchan.model.people.Person;

import java.util.List;

public class Retailer implements Person {
    private String name; //надати клієнтам карту лояльності
    private double cash;
    private List<Client> clients;

    public Retailer(String name, double cash, List<Client> clients) {
        this.name = name;
        this.cash = cash;
        this.clients = clients;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public void setClientsCard() {
    }
}
