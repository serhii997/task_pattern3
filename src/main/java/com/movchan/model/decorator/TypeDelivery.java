package com.movchan.model.decorator;

public enum TypeDelivery {
    COURIER, PICKUP
}
