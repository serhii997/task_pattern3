package com.movchan.model.people.domain;

import com.movchan.model.people.Cards;
import com.movchan.model.people.Person;

public class Client implements Person {
    private String name;
    private double cash;
    private Cards card;

    public Client(String name, double cash) {
        this.name = name;
        this.cash = cash;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public Cards getCard() {
        return card;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", cash=" + cash +
                ", card=" + card +
                '}';
    }
}
