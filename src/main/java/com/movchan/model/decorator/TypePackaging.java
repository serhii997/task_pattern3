package com.movchan.model.decorator;

public enum TypePackaging {
    TAPE, FLOWER_IN_BOX;
}
